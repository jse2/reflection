public class Description {

    public Description(String parameterName, String parameterTypeName, Boolean hasValue) {
        this.parameterName = parameterName;
        this.parameterTypeName = parameterTypeName;
        this.hasValue = hasValue;
    }

    private String parameterName;

    private String parameterTypeName;

    private Boolean hasValue;

    public String getParameterName() {
        return parameterName;
    }

    public String getParameterTypeName() {
        return parameterTypeName;
    }

    public Boolean getHasValue() {
        return hasValue;
    }

}
