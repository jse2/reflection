import domain.Person;

import java.time.LocalDate;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IllegalAccessException {

        List<Object> persons = List.of(new Person()
                , new Person("TEST", "TEST", LocalDate.now(), "TEST")
                , new Person("TEST", null, null, "TEST")
        );
        for (List<Description> descriptions : Extractor.getDescription(persons)) {
            System.out.println();
            for (Description description : descriptions) {
                System.out.println("parameterName: "
                        + description.getParameterName()
                        + ", parameterTypeName: "
                        + description.getParameterTypeName()
                        + ", hasValue: "
                        + description.getHasValue()
                );
            }
        }

        try {
            persons = List.of(new Person(), new Object());
            Extractor.getDescription(persons);
        } catch (IllegalArgumentException exception) {
            System.out.println(exception.getMessage());
        }

    }
}
