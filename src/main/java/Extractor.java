import java.lang.reflect.Field;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Extractor {

    public static List<List<Description>> getDescription(List<Object> objects) throws IllegalAccessException {
        if (objects.isEmpty()) {
            return Collections.emptyList();
        }

        Class<?> type = objects.get(0).getClass();
        for (Object object : objects) {
            if (!Objects.equals(type, object.getClass())) {
                throw new IllegalArgumentException("Distinct class");
            }
        }

        List<List<Description>> list = new LinkedList<>();
        for (Object object : objects) {
            List<Description> descriptions = new LinkedList<>();
            type = object.getClass();
            while (type != null) {
                descriptions.addAll(getDescriptionsFromClass(object, type));
                type = type.getSuperclass();
            }
            list.add(descriptions);
        }
        return list;
    }

    private static List<Description> getDescriptionsFromClass(Object object, Class<?> clazz) throws IllegalAccessException {
        List<Description> descriptions = new LinkedList<>();
        for (Field field : clazz.getDeclaredFields()) {
            Class<?> type = field.getClass();
            boolean canAccess = field.canAccess(object);
            Object value;
            if (!canAccess) {
                field.setAccessible(true);
                value = field.get(object);
            } else {
                value = field.get(object);
            }
            boolean hasValue;
            // если ссылка
            if (!type.isPrimitive()) {
                hasValue = value != null;
            } else if (type == boolean.class) { //если boolean
                hasValue = !Boolean.FALSE.equals(value);
            } else { //если все остальное
                hasValue = field.getInt(object) != 0;
            }
            if (!canAccess) {
                field.setAccessible(false);
            }
            descriptions.add(new Description(field.getName(), field.getType().getName(), hasValue));
        }
        return descriptions;
    }

}
